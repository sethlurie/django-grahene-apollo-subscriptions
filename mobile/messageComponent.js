import { gql, useSubscription, useQuery } from '@apollo/client'
import React from 'react';
import { Alert, Text, View } from 'react-native'


class MessageComponent extends React.Component<Props,State> {
  componentDidMount(){
    this.props.subscribeToMore()
  }

  render() {
    const { messages } = this.props
    return (
      <View>
        <Text>All messagers: </Text>
        {messages && messages.allMessages.map((element) => (<Text>{element.text}</Text>))}  
      </View>
    )
  }
}



const Message = ({ chatroom }) => {
  const MESSAGE_SUBSCRIPTION = gql`
  subscription messages($chatroom: String)
  {
    onNewMessage(chatroom: $chatroom) 
      { 
        text
        sender 
      }
  }
  `

  const ALL_MESSAGES = gql`
    query allMessages($chatroom:String){
      allMessages(chatroom:$chatroom) {
        text
        sender
      }
    }
    `
  const { subscribeToMore, data:messages } = useQuery(
    ALL_MESSAGES,
    {variables: { chatroom }}
  )
  const subscribe = () => subscribeToMore({
    document: MESSAGE_SUBSCRIPTION,
    variables: { chatroom },
    updateQuery: (prev, { subscriptionData }) => {
      if (!subscriptionData.data) return prev;
      const newMessage = subscriptionData.data.onNewMessage;
      return Object.assign({}, prev, {
        allMessages:[newMessage, ...prev.allMessages]
      });
    }
  })

  return (
    <MessageComponent
      messages={messages}
      subscribeToMore={subscribe}
    />
  )
}

export default Message
