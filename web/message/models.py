from django.db import models
from django.utils.translation import gettext_lazy as _

class Message(models.Model):
    chatroom = models.CharField(
        verbose_name = _("Room Name"),
        max_length = 125,
    )

    sender = models.CharField(
        verbose_name = _("Sender"),
        max_length = 125,
    )

    text = models.TextField(
        verbose_name = _("Message Text"),
    )

    def __str__(self):
        return self.text

    class Meta:
        verbose_name_plural = _("Messages")
