import channels_graphql_ws
import graphene
import graphene_django
from rx import Observable
from graphene_django.types import DjangoObjectType
from message.models import Message

class OnNewChatMessage(channels_graphql_ws.Subscription):
    """Simple GraphQL subscription."""

    # Subscription payload.
    sender = graphene.String()
    chatroom = graphene.String()
    text = graphene.String()

    class Arguments:
        """That is how subscription arguments are defined."""
        chatroom = graphene.String()

    @staticmethod
    def subscribe(self, info, chatroom=None):
        """Called when user subscribes."""

        # Return the list of subscription group names.
        print(chatroom, "subscribed")
        return [chatroom] if chatroom is not None else None

    @staticmethod
    def publish(self, info, chatroom=None):
        # The `self` contains payload delivered from the `broadcast()`.
        new_msg_chatroom = self["chatroom"]
        new_msg_text = self["text"]
        new_msg_sender = self["sender"]

        # Method is called only for events on which client explicitly
        # subscribed, by returning proper subscription groups from the
        # `subscribe` method. So he either subscribed for all events or
        # to particular chatroom.
        assert chatroom is None or chatroom == new_msg_chatroom

        # Avoid self-notifications.
        # if (
        #     info.context.user.is_authenticated
        #     and new_msg_sender == info.context.user.username
        # ):
        #     return OnNewChatMessage.SKIP

        return OnNewChatMessage(
            chatroom=chatroom, text=new_msg_text, sender=new_msg_sender
        )
    
    @classmethod
    def new_chat_message(cls, chatroom, text, sender):
        """Auxiliary function to send subscription notifications.
        It is generally a good idea to encapsulate broadcast invocation
        inside auxiliary class methods inside the subscription class.
        That allows to consider a structure of the `payload` as an
        implementation details.
        """
        cls.broadcast(
            group=chatroom,
            payload={"chatroom": chatroom, "text": text, "sender": sender},
        )

class MessageType(DjangoObjectType):
    class Meta:
        model = Message

class ChatQuery(graphene.ObjectType):
    all_messages = graphene.List(
        MessageType,
        chatroom=graphene.String(),
    )
    def resolve_all_messages(self, info, chatroom=None):
        if not chatroom:
            return None
        else:
            return Message.objects.filter(chatroom=chatroom)

class Query( ChatQuery):
    """Root GraphQL query."""
    pass


class SendChatMessage(graphene.Mutation):
    """Send chat message."""

    ok = graphene.Boolean()

    class Arguments:
        """Mutation arguments."""

        chatroom = graphene.String()
        text = graphene.String()

    def mutate(self, info, chatroom, text):
        """Mutation "resolver" - store and broadcast a message."""

        # Use the username from connection when implemented
        username = "anon"

        # Store a message.
        Message.objects.create(
            chatroom=chatroom,
            sender=username,
            text=text
        )

        # Notify subscribers.
        OnNewChatMessage.new_chat_message(chatroom=chatroom, text=text, sender=username)

        return SendChatMessage(ok=True)

class Mutation(graphene.ObjectType):
    """Root GraphQL mutation."""
    send_message = SendChatMessage.Field()

class Subscription(graphene.ObjectType):
    """Root GraphQL subscription."""
    on_new_message = OnNewChatMessage.Field()

graphql_schema = graphene.Schema(
    query=Query,
    mutation=Mutation,
    subscription=Subscription,
)